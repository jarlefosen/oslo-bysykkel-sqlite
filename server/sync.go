package server

import (
	"context"
	"fmt"
	"log/slog"

	"github.com/jarlefosen/oslobysykkel-sqlite/db"
	"github.com/jarlefosen/oslobysykkel-sqlite/oslobysykkel"
)

func PopulateDatabase(ctx context.Context, client oslobysykkel.Client, database *db.Database) error {
	stations, err := client.GetStations(ctx)
	if err != nil {
		return err
	}

	stationStatus, err := client.GetStationAvailability(ctx)
	if err != nil {
		return err
	}

	var updatedStations = 0
	var updatedStationStatus = 0

	err = database.WithTransaction(ctx, func(conn db.ContextExecutor) error {
		for _, station := range stations.Data.Stations {
			if err := db.UpsertStation(ctx, conn, &db.Station{
				ID:          station.ID,
				Name:        station.Name,
				Address:     station.Address,
				CrossStreet: station.CrossStreet,
				Lat:         station.Lat,
				Lon:         station.Lon,
				Capacity:    station.Capacity,
			}); err != nil {
				return fmt.Errorf("failed to upsert station for id %s: %w", station.ID, err)
			}
			updatedStations++
		}
		for _, stationStatus := range stationStatus.Data.Stations {
			if err := db.UpsertStationStatus(ctx, conn, &db.StationStatus{
				StationID:         stationStatus.StationID,
				NumBikesAvailable: stationStatus.NumBikesAvailable,
				NumDocksAvailable: stationStatus.NumDocksAvailable,
				IsInstalled:       stationStatus.IsInstalled,
				IsRenting:         stationStatus.IsRenting,
				IsReturning:       stationStatus.IsReturning,
				LastReported:      stationStatus.LastReported,
			}); err != nil {
				return fmt.Errorf("failed to upsert station status for id %s: %w", stationStatus.StationID, err)
			}
			updatedStationStatus++
		}
		return nil
	})
	if err != nil {
		return err
	}
	slog.Debug("Populated database executed without error", slog.Int("stations", updatedStations), slog.Int("station_status", updatedStationStatus))
	return nil
}
