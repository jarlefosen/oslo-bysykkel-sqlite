package main

import (
	"context"
	"errors"
	"log/slog"
	"os"
	"os/signal"

	"github.com/jarlefosen/oslobysykkel-sqlite/db"
)

func run(ctx context.Context, dbFilePath string, args []string) error {
	if dbFilePath == "" {
		return errors.New("dbFilePath is empty")
	}

	if len(args) == 0 {
		return errors.New("no arguments provided")
	}

	database, err := db.OpenDB(ctx, dbFilePath)
	if err != nil {
		return err
	}
	defer database.Close()

	switch args[0] {
	case "up":
		return db.MigrateUp(ctx, dbFilePath)
	case "down-1":
		return db.MigrateDown(ctx, dbFilePath, 1)
	case "status":
		version, err := db.GetSchemaVersion(ctx, dbFilePath)
		if err != nil {
			return err
		}
		slog.Info("Migration status", slog.Int("version", version.Version), slog.Bool("dirty", version.Dirty))
		return nil
	default:
		return errors.New("invalid argument")
	}
}

func envOrDefault(key, def string) string {
	if v := os.Getenv(key); v != "" {
		return v
	}
	return def
}

func main() {
	dbFilepath := envOrDefault("DB_FILEPATH", db.DefaultDBFile)
	if dbFilepath == "" {
		slog.Error("DB_FILEPATH is not set")
		os.Exit(1)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer cancel()
	if err := run(ctx, dbFilepath, os.Args[1:]); err != nil {
		slog.Error("Error running database migrations", slog.String("error", err.Error()))
		os.Exit(1)
	}
}
