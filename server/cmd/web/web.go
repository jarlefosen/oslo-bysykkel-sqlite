package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	server "github.com/jarlefosen/oslobysykkel-sqlite"
	"github.com/jarlefosen/oslobysykkel-sqlite/db"
	"github.com/jarlefosen/oslobysykkel-sqlite/oslobysykkel"
	"github.com/jarlefosen/oslobysykkel-sqlite/rest"
)

var logLevelMap = map[string]slog.Level{
	"DEBUG": slog.LevelDebug,
	"INFO":  slog.LevelInfo,
	"WARN":  slog.LevelWarn,
	"ERROR": slog.LevelError,
}

var (
	LogLevel     = envOrDefault("LOG_LEVEL", "INFO")
	CertFilePath = envOrDefault("CERT_FILE", "")
	KeyFilePath  = envOrDefault("KEY_FILE", "")
	port         = envOrDefault("PORT", "8080")

	osloBysykkelClientIdentifier = envOrDefault("OSLO_BYSYKKEL_CLIENT_ID", "test-bysykkelui")
	dbFileName                   = envOrDefault("DB_FILEPATH", db.DefaultDBFile)
	populateDatabaseOnStartup    = envOrDefault("POPULATE_DATABASE_ON_STARTUP", "true")
	continuousDatabaseUpdates    = envOrDefault("CONTINUOUS_DATABASE_UPDATES", "true")
)

func envOrDefault(key, def string) string {
	if v := os.Getenv(key); v != "" {
		return v
	}
	return def
}

func runDatabasePopulationLoopfunc(ctx context.Context, bysykkelClient oslobysykkel.Client, database *db.Database) {
	ticker := time.NewTicker(15 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			updateStart := time.Now()
			slog.Debug("Updating database")
			if err := server.PopulateDatabase(ctx, bysykkelClient, database); err != nil {
				slog.Error("Failed to update database", slog.String("error", err.Error()))
			}
			slog.Debug("Database updated", slog.Duration("duration", time.Since(updateStart)))
		}
	}
}

func runHttpServerfunc(s *http.Server, tlsConfig *tls.Config, ch chan<- error) {
	defer close(ch)
	if tlsConfig != nil {
		slog.Info("Starting web server with TLS", slog.String("port", port), slog.String("host", "https://localhost:"+port))
		if err := s.ListenAndServeTLS(CertFilePath, KeyFilePath); err != nil {
			ch <- err
		}
	} else {
		slog.Info("Starting web server", slog.String("port", port), slog.String("host", "http://localhost:"+port))
		if err := s.ListenAndServe(); err != nil {
			ch <- err
		}
	}
}

func run(ctx context.Context) error {

	bysykkelClient, err := oslobysykkel.New(osloBysykkelClientIdentifier)
	if err != nil {
		return fmt.Errorf("failed to create bysykkel client: %w", err)
	}

	if err := db.MigrateUp(ctx, dbFileName); err != nil {
		return fmt.Errorf("failed to migrate database %s: %w", dbFileName, err)
	}

	database, err := db.NewDatabase(ctx, dbFileName)
	if err != nil {
		return fmt.Errorf("failed to open database %s: %w", dbFileName, err)
	}

	if populateDatabaseOnStartup == "true" {
		slog.Debug("Populating database on startup")
		populationStart := time.Now()
		if err := server.PopulateDatabase(ctx, bysykkelClient, database); err != nil {
			return fmt.Errorf("failed to populate database on startup: %w", err)
		}
		slog.Info("Bysykkel database populated", slog.Duration("duration", time.Since(populationStart)))
	}

	var tlsConfig *tls.Config = nil
	if KeyFilePath != "" {
		tlsConfig = &tls.Config{
			MinVersion: tls.VersionTLS12,
		}
	}

	r := rest.CreateRouter(database)
	s := &http.Server{
		TLSConfig: tlsConfig,
		Addr:      ":" + port,
		Handler:   r,
	}

	if continuousDatabaseUpdates == "true" {
		go runDatabasePopulationLoopfunc(ctx, bysykkelClient, database)
	}

	httpErrChan := make(chan error)
	go runHttpServerfunc(s, tlsConfig, httpErrChan)

	select {
	case <-ctx.Done():
		slog.Info("Shutting down web server from signal", slog.String("signal", ctx.Err().Error()))
		shutdownctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		if err := s.Shutdown(shutdownctx); err != nil {
			s.Close()
			return err
		}
		return s.Close()
	case err, hasValue := <-httpErrChan:
		if !hasValue {
			return nil
		}
		return fmt.Errorf("failed to start http server: %w", err)
	}

}

func main() {
	if level, ok := logLevelMap[strings.ToUpper(LogLevel)]; ok {
		slog.SetLogLoggerLevel(level)
	} else {
		slog.Error("Invalid log level", slog.String("logLevel", LogLevel))
	}

	slog.SetLogLoggerLevel(slog.LevelDebug)

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer cancel()
	if err := run(ctx); err != nil {
		slog.Error("Error running web server", slog.String("error", err.Error()))
		cancel()
		os.Exit(1)
	}
}
