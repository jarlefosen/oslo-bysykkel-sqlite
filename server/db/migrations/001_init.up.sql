CREATE TABLE stations (
  id TEXT PRIMARY KEY,
  name TEXT NOT NULL,
  address TEXT,
  cross_street TEXT,
  latitude DOUBLE PRECISION NOT NULL,
  longitude DOUBLE PRECISION NOT NULL,
  capacity INTEGER NOT NULL
);

CREATE TABLE station_status (
    station_id TEXT PRIMARY KEY,
    num_bikes_available INTEGER NOT NULL,
    num_docks_available INTEGER NOT NULL,
    is_installed BOOLEAN NOT NULL,
    is_renting BOOLEAN NOT NULL,
    is_returning BOOLEAN NOT NULL,
    last_reported TIMESTAMP NOT NULL
);
