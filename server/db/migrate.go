package db

import (
	"context"
	"database/sql"
	"embed"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/sqlite"
	"github.com/golang-migrate/migrate/v4/source/iofs"

	_ "modernc.org/sqlite"
)

// Embed migrations in directory ./migrations/*.sql
//
//go:embed migrations/*.sql
var migrations embed.FS

func CreateDatabaseMigrator(db *sql.DB) (*migrate.Migrate, error) {
	driver, err := sqlite.WithInstance(db, &sqlite.Config{})
	if err != nil {
		return nil, fmt.Errorf("failed to create sqlite driver instance: %w", err)
	}

	resource, err := iofs.New(migrations, "migrations")
	if err != nil {
		return nil, fmt.Errorf("failed to create source resource instance: %w", err)
	}

	m, err := migrate.NewWithInstance("iofs", resource, "sqlite", driver)
	if err != nil {
		return nil, fmt.Errorf("failed to create migration instance: %w", err)
	}

	return m, nil

}

func MigrateUp(ctx context.Context, dbFilepath string) error {
	db, err := OpenDB(ctx, dbFilepath)
	if err != nil {
		return err
	}
	defer db.Close()

	m, err := CreateDatabaseMigrator(db)
	if err != nil {
		return err
	}
	defer m.Close()

	if err := m.Up(); err != nil {
		switch err {
		case migrate.ErrNoChange:
			return nil
		default:
			return fmt.Errorf("failed to apply migrations: %w", err)
		}
	}

	return nil
}

func MigrateDown(ctx context.Context, dbFilepath string, numSteps int) error {
	if numSteps <= 0 {
		return fmt.Errorf("invalid number of steps, must be a positive integer")
	}

	db, err := OpenDB(ctx, dbFilepath)
	if err != nil {
		return err
	}
	defer db.Close()

	m, err := CreateDatabaseMigrator(db)
	if err != nil {
		return err
	}
	defer m.Close()

	if err := m.Steps(-numSteps); err != nil {
		switch err {
		case migrate.ErrNoChange:
			return nil
		default:
			return fmt.Errorf("failed to apply migrations: %w", err)
		}
	}

	return nil
}

type SchemaVersion struct {
	Version int
	Dirty   bool
}

func GetSchemaVersion(ctx context.Context, dbFilepath string) (*SchemaVersion, error) {
	db, err := OpenDB(ctx, dbFilepath)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	m, err := CreateDatabaseMigrator(db)
	if err != nil {
		return nil, err
	}
	defer m.Close()

	version, dirty, err := m.Version()
	if err != nil {
		return nil, fmt.Errorf("failed to get schema version: %w", err)

	}

	return &SchemaVersion{
		Version: int(version),
		Dirty:   dirty,
	}, nil

}

func SetSchemaVersion(ctx context.Context, dbFilepath string, version int) error {
	db, err := OpenDB(ctx, dbFilepath)
	if err != nil {
		return err
	}
	defer db.Close()

	m, err := CreateDatabaseMigrator(db)
	if err != nil {
		return err
	}
	defer m.Close()

	return m.Force(version)
}
