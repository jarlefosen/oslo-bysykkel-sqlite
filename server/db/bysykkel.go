package db

import (
	"context"
	"fmt"
)

// Database model for SQLite containing station information
type Station struct {
	ID          string
	Name        string
	Address     string
	CrossStreet string
	Lat         float64
	Lon         float64
	Capacity    int
}

type StationStatus struct {
	StationID         string
	IsInstalled       bool
	IsRenting         bool
	IsReturning       bool
	LastReported      int // UTC timestamp in seconds
	NumBikesAvailable int
	NumDocksAvailable int
}

func ListStations(ctx context.Context, conn ContextExecutor) ([]*Station, error) {
	rows, err := conn.QueryContext(ctx, "SELECT id, name, address, cross_street, latitude, longitude, capacity FROM stations")
	if err != nil {
		return nil, fmt.Errorf("failed to query for stations: %w", err)
	}
	defer rows.Close()

	stations := make([]*Station, 0)
	for rows.Next() {
		var s Station
		if err := rows.Scan(&s.ID, &s.Name, &s.Address, &s.CrossStreet, &s.Lat, &s.Lon, &s.Capacity); err != nil {
			return nil, fmt.Errorf("failed to scan station: %w", err)
		}
		stations = append(stations, &s)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to scan stations: %w", err)
	}
	return stations, nil

}

func ListStationStatus(ctx context.Context, conn ContextExecutor) ([]*StationStatus, error) {
	rows, err := conn.QueryContext(ctx, "SELECT station_id, num_bikes_available, num_docks_available, is_installed, is_renting, is_returning, last_reported FROM station_status")
	if err != nil {
		return nil, fmt.Errorf("failed to query for station status: %w", err)
	}
	defer rows.Close()
	stationStatuses := make([]*StationStatus, 0)
	for rows.Next() {
		var s StationStatus
		if err := rows.Scan(&s.StationID, &s.NumBikesAvailable, &s.NumDocksAvailable, &s.IsInstalled, &s.IsRenting, &s.IsReturning, &s.LastReported); err != nil {
			return nil, fmt.Errorf("failed to scan station status: %w", err)
		}

		stationStatuses = append(stationStatuses, &s)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to scan station status: %w", err)
	}
	return stationStatuses, nil
}

func DeleteStation(ctx context.Context, conn ContextExecutor, stationID string) error {
	_, err := conn.ExecContext(ctx, "DELETE FROM stations WHERE id = ?", stationID)
	if err != nil {
		return fmt.Errorf("failed to delete station: %w", err)
	}
	return nil
}

func UpsertStation(ctx context.Context, conn ContextExecutor, station *Station) error {
	_, err := conn.ExecContext(ctx,
		`INSERT INTO stations (id, name, address, cross_street, latitude, longitude, capacity)
		VALUES (?, ?, ?, ?, ?, ?, ?)
		ON CONFLICT DO UPDATE SET name = excluded.name, address = excluded.address, cross_street = excluded.cross_street, latitude = excluded.latitude, longitude = excluded.longitude, capacity = excluded.capacity
		`, station.ID, station.Name, station.Address, station.CrossStreet, station.Lat, station.Lon, station.Capacity)
	if err != nil {
		return fmt.Errorf("failed to upsert station: %w", err)
	}
	return nil
}

func InsertStation(ctx context.Context, conn ContextExecutor, station *Station) error {
	_, err := conn.ExecContext(ctx, "INSERT INTO stations (id, name, address, cross_street, latitude, longitude, capacity) VALUES (?, ?, ?, ?, ?, ?, ?)", station.ID, station.Name, station.Address, station.CrossStreet, station.Lat, station.Lon, station.Capacity)
	if err != nil {
		return fmt.Errorf("failed to insert station: %w", err)
	}
	return nil
}

func DeleteStationStatus(ctx context.Context, conn ContextExecutor, stationID string) error {
	_, err := conn.ExecContext(ctx, "DELETE FROM station_status WHERE station_id = ?", stationID)
	if err != nil {
		return fmt.Errorf("failed to delete station status: %w", err)
	}
	return nil
}

func UpsertStationStatus(ctx context.Context, conn ContextExecutor, stationStatus *StationStatus) error {
	_, err := conn.ExecContext(ctx,
		`INSERT INTO station_status (station_id, num_bikes_available, num_docks_available, is_installed, is_renting, is_returning, last_reported)
		VALUES (?, ?, ?, ?, ?, ?, ?)
		ON CONFLICT DO UPDATE SET num_bikes_available = excluded.num_bikes_available, num_docks_available = excluded.num_docks_available, is_installed = excluded.is_installed, is_renting = excluded.is_renting, is_returning = excluded.is_returning, last_reported = excluded.last_reported
		`, stationStatus.StationID, stationStatus.NumBikesAvailable, stationStatus.NumDocksAvailable, stationStatus.IsInstalled, stationStatus.IsRenting, stationStatus.IsReturning, stationStatus.LastReported)
	if err != nil {
		return fmt.Errorf("failed to upsert station status: %w", err)
	}
	return nil
}

func InsertStationStatus(ctx context.Context, conn ContextExecutor, stationStatus *StationStatus) error {
	_, err := conn.ExecContext(ctx,
		`INSERT INTO station_status (station_id, num_bikes_available, num_docks_available, is_installed, is_renting, is_returning, last_reported) VALUES (?, ?, ?, ?, ?, ?, ?)`,
		stationStatus.StationID, stationStatus.NumBikesAvailable, stationStatus.NumDocksAvailable, stationStatus.IsInstalled, stationStatus.IsRenting, stationStatus.IsReturning, stationStatus.LastReported)
	if err != nil {
		return fmt.Errorf("failed to insert station status: %w", err)
	}
	return nil
}
