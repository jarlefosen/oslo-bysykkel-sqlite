package db

import (
	"context"
	"database/sql"
	"fmt"

	_ "modernc.org/sqlite"
)

const DefaultDBFile = "bysykkel.db"

type ContextExecutor interface {
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
}

type Database struct {
	db *sql.DB
}

func NewDatabase(ctx context.Context, filename string) (*Database, error) {
	db, err := OpenDB(ctx, filename)
	if err != nil {
		return nil, err
	}
	return &Database{db: db}, nil
}

func OpenDB(ctx context.Context, filename string) (*sql.DB, error) {
	db, err := sql.Open("sqlite", filename)
	if err != nil {
		return nil, fmt.Errorf("failed to open database file %s: %w", filename, err)
	}
	if err := db.PingContext(ctx); err != nil {
		return nil, fmt.Errorf("failed to ping database: %w", err)
	}

	db.ExecContext(ctx, `
	PRAGMA foreign_keys = ON;
	PRAGMA journal_mode = WAL;
	PRAGMA cache_size = 10000;
	PRAGMA temp_store = MEMORY;
	PRAGMA busy_timeout = 5000;
	PRAGMA encoding = "UTF-8";
	PRAGMA foreign_keys = ON;
	`)

	return db, nil
}

func (db *Database) GetConnection() ContextExecutor {
	return db.db
}

func (db *Database) WithTransaction(ctx context.Context, fn func(conn ContextExecutor) error) error {
	tx, err := db.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to begin transaction: %w", err)
	}
	defer tx.Rollback()

	if err := fn(tx); err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("failed to commit transaction: %w", err)
	}

	return nil
}

func (db *Database) Close() error {
	return db.db.Close()
}
