# Oslo Bysykkel - Server

The server component is a slim system that polls Oslo Bysykkel for station
information and availability, and persists the information in a SQLite database.

The server also offers a slim API for requesting Station information and Station availability.

## Running the application

The application requires go 1.22.2 or newer to run the app.

```
go run cmd/web/web.go
```

The application defaults to running on port `:8080` and performs necessary
database migrations automatically.

### Configuration possibilities

Environment variables

| ENVIRONMENT NAME             | Description                                                                 | Default         |
| ---------------------------- | --------------------------------------------------------------------------- | --------------- |
| PORT                         |                                                                             | 8080            |
| LOG_LEVEL                    | Choose the log level you are interested in seeing. DEBUG, INFO, WARN, ERROR | INFO            |
| POPULATE_DATABASE_ON_STARTUP | Whether to run a database population before the application starts          | true            |
| CONTINUOUS_DATABASE_UPDATES  | Whether the application should fetch new station status every 15 seconds    | true            |
| OSLO_BYSYKKEL_CLIENT_ID      | Client Identifier sent to Oslo Bysykkel API                                 | test-bysykkelui |
| CERT_FILE                    | Enable TLS by passing a CERT_FILE                                           |                 |
| KEY_FILE                     | Enable TLS by passing a KEY_FILE                                            |                 |

## Endpoints supported

List all stations

```
GET /station

200 OK
{
stations: [
    {
        id: string
        name: string
        address: string
        cross_street: string
        capacity: int
        lat: float
        lon: float
        }
    ]

}

```

List availability for all stations

```
GET /station-availability

200 OK
{
    stations: [
        {
            station_id: string
            is_installed: boolean
            is_renting: boolean
            is_returning: boolean
            last_reported: string (rfc-3339)
            num_bikes_available: int
            num_docks_available: int
        }
    ]
}

```
