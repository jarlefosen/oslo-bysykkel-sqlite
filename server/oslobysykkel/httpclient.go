package oslobysykkel

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strings"
	"time"
)

var ErrInvalidClientIdentifier = errors.New("client identifier must follow the format <orgName>-<appName>")

func validateClientIdentifier(clientIdentifier string) error {
	if clientIdentifier == "" {
		return ErrInvalidClientIdentifier
	}

	// Check if the client identifier follows the desired format "<orgName>-<appName>"
	// from the docs: https://oslobysykkel.no/apne-data/sanntid
	if parts := strings.Split(clientIdentifier, "-"); len(parts) != 2 || len(parts[0]) == 0 || len(parts[1]) == 0 {
		return ErrInvalidClientIdentifier
	}

	return nil
}

type OsloBysykkelClient struct {
	httpClient       *http.Client
	clientIdentifier string
}

var _ Client = &OsloBysykkelClient{}

func New(clientIdentifier string) (*OsloBysykkelClient, error) {
	if err := validateClientIdentifier(clientIdentifier); err != nil {
		return nil, err
	}

	return &OsloBysykkelClient{
		httpClient: &http.Client{
			Timeout: 60 * time.Second,
		},
		clientIdentifier: clientIdentifier,
	}, nil
}

func (c *OsloBysykkelClient) doRequest(ctx context.Context, req *http.Request) (*http.Response, error) {
	req.Header.Set("Client-Identifier", c.clientIdentifier)
	return c.httpClient.Do(req.WithContext(ctx))
}

func (c *OsloBysykkelClient) GetStations(ctx context.Context) (*StationResponse, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://gbfs.urbansharing.com/oslobysykkel.no/station_information.json", nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.doRequest(ctx, req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var stationResponse StationResponse
	if err := json.NewDecoder(resp.Body).Decode(&stationResponse); err != nil {
		return nil, err
	}

	return &stationResponse, nil
}

func (c *OsloBysykkelClient) GetStationAvailability(ctx context.Context) (*StationAvailabilityResponse, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://gbfs.urbansharing.com/oslobysykkel.no/station_status.json", nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.doRequest(ctx, req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var stationAvailabilityResponse StationAvailabilityResponse
	if err := json.NewDecoder(resp.Body).Decode(&stationAvailabilityResponse); err != nil {
		return nil, err
	}

	return &stationAvailabilityResponse, nil
}
