package oslobysykkel

import (
	"context"
)

type Client interface {
	GetStations(ctx context.Context) (*StationResponse, error)
	GetStationAvailability(ctx context.Context) (*StationAvailabilityResponse, error)
}

type StationResponse struct {
	LastUpdated int    `json:"last_updated"`
	TTL         int    `json:"ttl"`
	Version     string `json:"version"`
	Data        struct {
		Stations []Station `json:"stations"`
	} `json:"data"`
}

type Station struct {
	ID          string  `json:"station_id"`
	Name        string  `json:"name"`
	Address     string  `json:"address"`
	CrossStreet string  `json:"cross_street"`
	Lat         float64 `json:"lat"`
	Lon         float64 `json:"lon"`
	Capacity    int     `json:"capacity"`
}

type StationAvailabilityResponse struct {
	LastUpdated int    `json:"last_updated"`
	TTL         int    `json:"ttl"`
	Version     string `json:"version"`
	Data        struct {
		Stations []StationAvailability `json:"stations"`
	} `json:"data"`
}

type StationAvailability struct {
	StationID             string                `json:"station_id"`
	IsInstalled           bool                  `json:"is_installed"`
	IsRenting             bool                  `json:"is_renting"`
	IsReturning           bool                  `json:"is_returning"`
	LastReported          int                   `json:"last_reported"`
	NumVehiclesAvailable  int                   `json:"num_vehicles_available"`
	NumBikesAvailable     int                   `json:"num_bikes_available"`
	NumDocksAvailable     int                   `json:"num_docks_available"`
	VehicleTypesAvailable []VehicleAvailability `json:"vehicle_types_available"`
}

type VehicleAvailability struct {
	VehicleTypeID string `json:"vehicle_type_id"`
	Count         int    `json:"count"`
}
