package rest

import (
	"encoding/json"
	"log/slog"
	"net/http"
	"time"

	"github.com/jarlefosen/oslobysykkel-sqlite/db"
)

func CreateRouter(database *db.Database) http.Handler {
	mu := http.NewServeMux()
	mu.Handle("GET /stations", HandleListStations(database))
	mu.HandleFunc("OPTIONS /stations", allowCORSFunc)
	mu.Handle("GET /station-availability", HandleListAvailability(database))
	mu.HandleFunc("OPTIONS /stations-availability", allowCORSFunc)
	return mu
}

func allowCORSFunc(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.WriteHeader(http.StatusOK)
}

type Station struct {
	ID          string  `json:"id"`
	Name        string  `json:"name"`
	Address     string  `json:"address"`
	CrossStreet string  `json:"cross_street"`
	Lat         float64 `json:"lat"`
	Lon         float64 `json:"lon"`
	Capacity    int     `json:"capacity"`
}

type StationAvailability struct {
	StationID         string    `json:"station_id"`
	IsInstalled       bool      `json:"is_installed"`
	IsRenting         bool      `json:"is_renting"`
	IsReturning       bool      `json:"is_returning"`
	LastReported      time.Time `json:"last_reported"`
	NumBikesAvailable int       `json:"num_bikes_available"`
	NumDocksAvailable int       `json:"num_docks_available"`
}

type ListAvailabilityResponse struct {
	Stations []StationAvailability `json:"stations"`
}

type ListStationsResponse struct {
	Stations []Station `json:"stations"`
}

func HandleListStations(database *db.Database) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		defer r.Body.Close()
		stationsInfo, err := db.ListStations(r.Context(), database.GetConnection())
		if err != nil {
			slog.Error("failed to list stations", slog.String("error", err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("failed to list stations"))
			return
		}

		stations := make([]Station, 0, len(stationsInfo))
		for _, station := range stationsInfo {
			stations = append(stations, Station{
				ID:          station.ID,
				Name:        station.Name,
				Address:     station.Address,
				CrossStreet: station.CrossStreet,
				Lat:         station.Lat,
				Lon:         station.Lon,
				Capacity:    station.Capacity,
			})
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(ListStationsResponse{
			Stations: stations,
		}); err != nil {
			slog.Error("failed to encode stations", slog.String("error", err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

	}
}

func HandleListAvailability(database *db.Database) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		defer r.Body.Close()

		conn := database.GetConnection()
		statuses, err := db.ListStationStatus(r.Context(), conn)
		if err != nil {
			slog.Error("failed to list station availability", slog.String("error", err.Error()))

			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("failed to list station availability"))
			return
		}

		stations := make([]StationAvailability, 0, len(statuses))
		for _, status := range statuses {
			stations = append(stations, StationAvailability{
				StationID:         status.StationID,
				IsInstalled:       status.IsInstalled,
				IsRenting:         status.IsRenting,
				IsReturning:       status.IsReturning,
				LastReported:      time.Unix(int64(status.LastReported), 0),
				NumBikesAvailable: status.NumBikesAvailable,
				NumDocksAvailable: status.NumDocksAvailable,
			})
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		enc := json.NewEncoder(w)
		if err := enc.Encode(&ListAvailabilityResponse{
			Stations: stations,
		}); err != nil {
			slog.Error("failed to encode station availability", slog.String("error", err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}
