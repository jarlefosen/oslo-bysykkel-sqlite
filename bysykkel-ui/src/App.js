import { useEffect, useState } from "react";
import "./App.css";

import {
  CircleMarker,
  LayerGroup,
  MapContainer,
  Popup,
  TileLayer,
} from "react-leaflet";

const getStations = async () => {
  const stationsResponse = await fetch("http://localhost:8080/stations", {
    method: "GET",
    credentials: "omit",
    mode: "cors",
  });
  const stationsRes = await stationsResponse.json();
  return stationsRes.stations;
};

const getAvailability = async () => {
  const stationsResponse = await fetch(
    "http://localhost:8080/station-availability",
    {
      method: "GET",
      credentials: "omit",
      mode: "cors",
    }
  );
  const stationsRes = await stationsResponse.json();
  return stationsRes.stations;
};

const FilterAvailability = Object.freeze({
  AVAILABLE: "AVAILABLE",
  UNAVAILABLE: "UNAVAILABLE",
  ALL: "ALL",
});

function App() {
  const [ticker, setTicker] = useState(0);
  const [stations, setStations] = useState([]);
  const [availability, setAvailability] = useState({});

  const [lockFilter, setLockFilter] = useState(FilterAvailability.ALL);
  const [bikeFilter, setBikeFilter] = useState(FilterAvailability.ALL);

  useEffect(() => {
    const interval = setInterval(() => {
      setTicker((prev) => {
        return prev + 1;
      });
    }, 15000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    getStations().then((stations) => {
      console.debug("Stations list updated");
      setStations(stations);
    });
  }, []);

  useEffect(() => {
    getAvailability().then((availability) => {
      const availabilityMap = {};
      availability.forEach((station) => {
        availabilityMap[station.station_id] = station;
      });
      console.debug("Availability updated");
      setAvailability(availabilityMap);
    });
  }, [stations, ticker]);

  const [renderData, setRenderData] = useState([]);
  useEffect(() => {
    const data = stations
      .map((station) => {
        return {
          station,
          availability: availability[station.id],
        };
      })
      .filter((station) => {
        if (lockFilter === FilterAvailability.UNAVAILABLE) {
          return station.availability.num_docks_available === 0;
        } else if (lockFilter === FilterAvailability.AVAILABLE) {
          return station.availability.num_docks_available > 0;
        }
        return true;
      })
      .filter((station) => {
        if (bikeFilter === FilterAvailability.UNAVAILABLE) {
          return station.availability.num_bikes_available === 0;
        } else if (bikeFilter === FilterAvailability.AVAILABLE) {
          return station.availability.num_bikes_available > 0;
        }
        return true;
      });
    setRenderData(data);
  }, [stations, availability, bikeFilter, lockFilter]);

  return (
    <div
      className="App"
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div
            style={{
              padding: "10px",
              flexDirection: "row",
            }}
          >
            <button
              style={{
                background:
                  lockFilter === FilterAvailability.ALL ? "gray" : undefined,
              }}
              onClick={() => setLockFilter(FilterAvailability.ALL)}
            >
              No filter
            </button>
            <button
              style={{
                background:
                  lockFilter === FilterAvailability.AVAILABLE
                    ? "gray"
                    : undefined,
              }}
              onClick={() => setLockFilter(FilterAvailability.AVAILABLE)}
            >
              Available locks
            </button>
            <button
              style={{
                background:
                  lockFilter === FilterAvailability.UNAVAILABLE
                    ? "gray"
                    : undefined,
              }}
              onClick={() => setLockFilter(FilterAvailability.UNAVAILABLE)}
            >
              No locks
            </button>
          </div>

          <div
            style={{
              padding: "10px",
              flexDirection: "row",
            }}
          >
            <button
              style={{
                background:
                  bikeFilter === FilterAvailability.ALL ? "gray" : undefined,
              }}
              onClick={() => setBikeFilter(FilterAvailability.ALL)}
            >
              No filter
            </button>
            <button
              style={{
                background:
                  bikeFilter === FilterAvailability.AVAILABLE
                    ? "gray"
                    : undefined,
              }}
              onClick={() => setBikeFilter(FilterAvailability.AVAILABLE)}
            >
              Available Bikes
            </button>
            <button
              style={{
                background:
                  bikeFilter === FilterAvailability.UNAVAILABLE
                    ? "gray"
                    : undefined,
              }}
              onClick={() => setBikeFilter(FilterAvailability.UNAVAILABLE)}
            >
              No bikes
            </button>
          </div>
        </div>
      </div>
      <MapContainer
        style={{ height: "90vh", width: "90vw" }}
        center={[59.93, 10.75]}
        zoom={13}
        scrollWheelZoom={true}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />

        <LayerGroup>
          {renderData.map((data) => (
            <CircleMarker
              key={data.station.id}
              center={[data.station.lat, data.station.lon]}
              title={data.station.name}
              color={data.availability.num_docks_available > 0 ? "blue" : "red"}
              fillColor={
                data.availability.num_bikes_available > 0 ? "blue" : "red"
              }
              fillOpacity={0.7}
              opacity={0.7}
            >
              <Popup>
                <div>
                  <h2>{data.station.name}</h2>
                  <div>
                    <p>
                      Available bikes: {data.availability.num_bikes_available}
                    </p>
                    <p>
                      Available locks: {data.availability.num_docks_available}
                    </p>
                  </div>
                </div>
              </Popup>
            </CircleMarker>
          ))}
        </LayerGroup>
      </MapContainer>
    </div>
  );
}

export default App;
