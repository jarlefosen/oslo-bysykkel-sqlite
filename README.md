# Oslo Bysykkel - With SQLite

A small application for fetching, persisting, and serving Oslo Bysykkel station
availability data.

The server contains a small SQLite database which is used to persist current
state from Oslo Bysykkel and serve the information in a slimmer format to reduce
bandwith requirements.

The web application is a map which presents all stations and their current
availability of locks and bikes.
It is also possible to filter the points of interest based on whether you need
a lock or a bike.

## How to run the app?

### Start the server

First you have to start the backend server written in Go.

```sh
cd server
go run cmd/web/web.go
```

Check out the [`server/README.md`](server/README.md) for instructions to run.

By default the app listens to [port 8080](http://localhost:8080/)

### Start the webapp

Next up you can start the frontend application written in React.

```sh
cd bysykkel-ui
npm install
npm start
```

The application now listens to [port 3000](http://localhost:3000/)

Check out the [`bysykkel-ui/README.md`](bysykkel-ui/README.md) for instructions to run the application.
